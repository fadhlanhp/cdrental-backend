# CD Rental

Gitlab Link: https://gitlab.com/fadhlanhp/cdrental-backend

### Prerequisite

Make sure you have a database named `cdRental`.

Migrate database

```bash
$ php artisan migrate
```

I dont have any seeding for this data, so please add some to database before you test any function in this api

### Installing

How to run this api:

```bash
$ php -S localhost:8000 -t ./public
```

### API Blueprint

```apib
## Data Structures

### Product
+ title: Kingdom (string, required)
+ rate: 1000 (number, required)
+ category: Romance (string, required) -- category should've been had its own table, but I didnt create one.
+ Quantity: 1 (number, required)

### Loan
+ userID: 1 (number, required)
+ productID: 1 (number, required)
+ loanDate: 2020-03-29 (string)
+ returnDate: 2020-04-01 (string)
+ price: 1000 (number)

## Product Blueprint

## Product [/products]
### List All Product [GET]
+ Response 200 (application/json)

    + Attributes (array[Product])

### Get A Product [GET]
+ Parameters

    + id: 1 (number, required)

+ Response 200 (application/json)

    + Attributes (Product)

### Create a New Product [POST]
+ Request (application/json)

    + Attributes (Product)

+ Response 200 (text/plain)

        Berhasil menambahkan CD ke database

### Update Product Quantity [PUT]
+ Parameters

    + id: 1 (number, required)

+ Request (application/json)

    + Attributes
        - Quantity: 4 (number, required)

+ Response 200 (text/plain)

    Berhasil mengubah quantity

## Loan [/loans]
### List All Loan Info [GET]
+ Response 200 (application/json)

    + Attributes (array[Loan])

### Get A Loan Info [GET]
+ Parameters

    + id: 1 (number, required)

+ Response 200 (application/json)

    + Attributes (Loan)

### Create a New Loan Transaction [POST]
+ Request (application/json)

    + Attributes (Loan)

+ Response 200 (text/plain)

        Berhasil menambahkan transaksi user ke database

### Finish Loan Transaction [PUT]
+ Parameters

    + id: 1 (number, required)

+ Response 200 (text/generated)

    Total harga yang harus dibayarkan oleh user sebesar (price)
```
