<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;



class LoanTest extends TestCase
{
    public function showAllLoans()
    {
        $this->get('/loans');
        $this->seeStatusCode(200);
    }

    public function showALoanTransaction()
    {
        $this->get('/loans/1');
        $this->seeStatusCode(200);
    }

    public function addALoanTransaction()
    {
        $parameters = [
            'userID' => '1',
            'productID' => '1',
        ];

        $this->post("loans", $parameters, []);
        $this->seeStatusCode(200);
    }

    public function finishLoanTransaction()
    {

        $this->put("loans/1", [], []);
        $this->seeStatusCode(200);
    }
}
