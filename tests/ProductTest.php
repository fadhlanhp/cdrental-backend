<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;



class ProductTest extends TestCase
{
    public function showAllProducts()
    {
        $this->get('/products');
        $this->seeStatusCode(200);
    }

    public function showAProducts()
    {
        $this->get('/products/1');
        $this->seeStatusCode(200);
    }

    public function addAProduct()
    {
        $parameters = [
            'title' => 'Battleship',
            'rate' => '1000',
            'category' => 'Action',
            'quantity' => '5'
        ];

        $this->post("products", $parameters, []);
        $this->seeStatusCode(200);
    }

    public function updateProductQuantity()
    {
        $parameters = [
            'quantity' => '4'
        ];

        $this->put("products/1", $parameters, []);
        $this->seeStatusCode(200);
    }
}
