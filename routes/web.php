<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/products', 'productController@index');
$router->get('/products/{id}', 'productController@show');
$router->post('/products', 'productController@addProduct');
$router->put('/products/{id}', 'productController@updateQuantity');

$router->get('/loans', 'loanController@index');
$router->get('/loans/{id}', 'loanController@show');
$router->post('/loans', 'loanController@addLoan');
$router->put('/loans/{id}', 'loanController@finishLoan');
