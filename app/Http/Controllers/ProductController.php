<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class ProductController extends BaseController
{
    public function index()
    {
        $data = Product::all();
        return response($data);
    }
    public function show($id)
    {
        $data = Product::where('id', $id)->get();
        return response($data);
    }
    public function addProduct(Request $request)
    {
        $data = new Product();
        $data->title = $request->input('title');
        $data->rate = $request->input('rate');
        $data->category = $request->input('category');
        $data->quantity = $request->input('quantity');
        $data->save();

        return response('Berhasil menambahkan CD ke database');
    }
    public function updateQuantity(Request $request, $id)
    {
        $data = Product::where('id', $id)->first();
        $data->quantity = $request->input('quantity');
        $data->save();

        return response('Berhasil mengubah quantity');
    }
}
