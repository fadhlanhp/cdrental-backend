<?php

namespace App\Http\Controllers;

use App\Product;
use App\Loan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class LoanController extends BaseController
{
    public function index()
    {
        $data = Loan::all();
        return response($data);
    }
    public function show($id)
    {
        $data = Loan::where('id', $id)->get();
        return response($data);
    }
    public function addLoan(Request $request)
    {
        $productID = $request->input('productID');
        $product = Product::where('id', $productID)->first();
        if ($product->quantity > 0) {
            $data = new Loan();
            $data->userID = $request->input('userID');
            $data->productID = $productID;
            $data->loanDate = Carbon::now();
            $data->save();

            $product->quantity = $product->quantity - 1;
            $product->save();

            return response('Berhasil menambahkan transaksi user ke database');
        } else {
            return response('CD tidak tersedia');
        }
    }
    public function finishLoan(Request $request, $id)
    {
        $data = Loan::where('id', $id)->first();
        $productID = $data->productID;
        $product = Product::where('id', $productID)->first();
        $product->quantity = $product->quantity + 1;
        $product->save();

        $data->returnDate = Carbon::now();
        $diff = $data->returnDate->diffInDays($data->loanDate);

        $price = $diff * $product->rate;
        $data->totalPrice = $price;
        $data->save();

        return response("Total harga yang harus dibayarkan oleh user sebesar {$price}");
    }
}
